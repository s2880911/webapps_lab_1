package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    Map<String, Double> quMap;

    public Quoter() {
        quMap = new HashMap<>();
        quMap.put("1", 10.0);
        quMap.put("2", 45.0);
        quMap.put("3", 20.0);
        quMap.put("4", 35.0);
        quMap.put("5", 50.0);
    }


    public double getBookPrice(String isbn) {
        for (String key : quMap.keySet()) {
            if (key.equals(isbn)) {
                return quMap.get(isbn);
            }
        }
        return 0.0;
    }

}
